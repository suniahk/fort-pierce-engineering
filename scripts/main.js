$(function() {
  function getQueryVariable(variable)
  {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split("=");
                if(pair[0] == variable){return pair[1];}
        }
        return(false);
  }

  $( 'nav ul' ).singlePageNav();
console.log( getQueryVariable( "sent" ) );
  if( getQueryVariable( "sent" ) === "1" ) {
    $( "#contactResults" ).toggle().css( "background-color", "#94FF94" ).text( "Your message was sent successfully!" );
  } else if  ( getQueryVariable( "sent" ) === "0" ) {
    $( "#contactResults" ).toggle().css( "background-color", "#FF9494" ).text( "There was a problem sending your message.  Please try again." );
  }
});
